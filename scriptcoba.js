//tugas 1:

for (let i = 1; i < 10; i++){
	for (let j=i; j>1; j--){
		document.write("*");
	}
	document.write("<br>");
}

//tugas 2:
for (let i = 1; i < 10; i++){
	for (let j = i; j < 10; j++){
		document.write("&nbsp");
	}
	for (let k = 1; k < i; k++){
		document.write("*");
	}
	document.write("<br>");
}

//tugas 3:

for (let i = 1; i < 10; i++){
	for (let j = i; j < 10; j++){
		document.write("&nbsp");
	}
	for (let k = 1; k < i; k++){
		document.write("*");
	}
	for (let m = 1; m < i-1; m++){
		document.write("*");
	}
	document.write("<br>");
}